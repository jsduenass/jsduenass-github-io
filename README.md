# personal website

##  visit the web site 👉 [here](https://jsduenass.github.io/)

## About
This repository contains a hugo static web site,a personal blog to share content. 

It is based on the project [hugo-toha.github.io](https://github.com/hugo-toha/hugo-toha.github.io), an example hugo static site with Toha theme.

This project uses the toha theme submodule. So in order to use this repository it must be recursively clone for it to contain the needed submodule.

git clone --recursive [URL to Git repo]

the following commands are used to  preview the website 
```
hugo server 
hugo server -t toha -w
```

Attributions:
- <a href='https://www.freepik.com/vectors/business'>Business vector created by studiogstock - www.freepik.com</a>
