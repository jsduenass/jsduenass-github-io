---
title: "Quotebook"
date: 2020-10-28
menu:
  sidebar:
    name: "Quotebook"
    identifier: quotebook
    parent: library
    weight: 21

---
This is a collection of quotes I found interesting in one way or another.

## Literature

> The fact we live at the bottom of a deep gravity well, on the surface of a gas covered planet going around a nuclear fireball 90 million miles away and think this to be normal is obviously some indication of how skewed our perspective tends to be. 
>
> __--- Douglas Adams__


> Así también un orbe entero, con toda su pequeñez y su grandeza, puede ubicarse en una estrella que titila en la inmensidad del espacio, y así como la mera ciencia humana es capaz de dividir un rayo de luz y analizar su composición, así tambien otras inteligencias más excelsas podrán leer acaso en el tenue brillar de esta tierra nuestra todo pensamiento y toda acción, todo vicio y toda virtud de cada uno de los seres responsables que la pueblan.
>
> __--- Charles Dickens, Historia de dos ciudades. pag. 212 (1859)__


>Bajo esa bóveda tachonada de luminarias fijas y eternas, tan lejanas algunas de este minúsculo planeta que según afirman los sabios es dudoso que sus rayos hallan tenido tiempo de llegar a el descubrirlo como un punto en el espacio donde nada se sufre y nada se hace.
>
> __--- Charles Dickens, Historia de dos ciudades - El zapatero. (1859)__

De las anteriores dos citas se evidencia el conocimiento de astronomía que poseía Charles Dickens, y como lo influía a contemplar un punto de vista más allá de la tierra, donde todo lo que conocemos es minúsculo. 

### A pale blue dot
{{< youtube EWPFmdAWRZ0 >}}

>From this distant vantage point, the Earth might not seem of any particular interest. 
>
>But for us, it's different. Look again at that dot. That's here. That's home. That's us. On it everyone you love, everyone you know, everyone you ever heard of, every human being who ever was, lived out their lives. The aggregate of our joy and suffering, thousands of confident religions, ideologies, and economic doctrines, every hunter and forager, every hero and coward, every creator and destroyer of civilization, ever king and peasant, every young couple in love, every mother and father, hopeful child, inventor and explorer, every teacher of morals, every corrupt politician, every "superstar,” every "supreme leader," every saint and sinner in the history of our species lived there on a mote of dust suspended in a sunbeam.
>
>The Earth is a very small stage in a vast cosmic arena. Think of the rivers of blood spilled by all those generals and emperors so that, in glory and triumph, they could become momentary masters of a fraction of a dot. Think of the endless cruelties visited by the inhabitants of one corner of this pixel the scarcely distinguishable inhabitants of some other corner, how frequent their misunderstandings, how eager they are to kill one another, how fervent their hatreds.
>
> Our posturings, our imagined self-importance, the delusion that we have some privileged position in the Universe, are challenged by this point of pale light. Our planet is a lonely speck in the great enveloping cosmic dark. In our obscurity, in all this vastness, there is no hint that help will come from elsewhere to save us from ourselves.
>The Earth is the only world known so far to harbor life. There is nowhere else, at least in the near future, to which our species could migrate. Visit, yes. Settle, not yet. Like it or not, for the moment the Earth is where we make our stand.
>
>It has been said that astronomy is a humbling and character-building experience. There is perhaps no better demonstration of the folly of human conceits than this distant image of our tiny world. To me, it underscores our responsibility to deal more kindly with one another, and to preserve and cherish the pale blue dot, the only home we've ever known.
>
> __--- Carl Sagan, A pale blue dot__

> For the very reason that here nothing served a purpose -not ever, not to anyone- and that here no guillotine of evolution was in play, nature, constrained neither by the life she bore nor by the death she inflicted, could achieve liberation, displaying a prodigality characteristic of herself, a limitless wastefulness, a brute magnificence that was useless, a eternal power of creation without a goal, without a need, without a meaning. 
> 
> __--- Stanislaw Lem, Fiasco__



> Es un sentimiento creciente entre algunos grupos de este país la noción de que cuando un hombre o una compañía han sacado un beneficio del publico durante un cierto número de años, el gobierno y los tribunales tienen el deber de salvaguardar esos beneficios en el futuro, incluso frente a circunstancias de cambio y contra el interés del publico... 
> Ni los individuos ni las corporaciones tienen el menor derecho de acudir a los tribunales y exigir que el reloj de la historia sea detenido, o retrasado, en beneficio particular suyo.
> 
> __---Robert.A Heinlein, La línea de la vida__

> The intensity of every astonishment gradually wear off; the human mind, by the sheer effect of repetition and habit, gradually becomes accustomed to the strangest and least familiar ideas.
>
> __--- Milic Capek, The philosophical impact of contemporary physics__

> El coronel experimentó la sensación de que nacían hongos y lirios venenosos en sus tripas.
>
> __--- Gabriel Garcia Marquez, El coronel no tiene quien le escriba__

Antítesis entre la belleza de los lirios y el malestar que representan.

> Fue en aquella cabaña perdida en las montañas del Gran Khingam donde algo comenzó a cambiar en el interior de Ye Wenjie. Las negras profundidades de la tundra congelada de su corazón comenzaron a deshelarse. Apareció una pequeña laguna de aguas cristalinas. 
>
> __--- Liu Cixin, El problema de los 3 cuerpos__

> Rostov, poco a poco ante la presencia de Berg, poco grata para el, adoptó de nuevo el tono anterior de húsar valentón, y animándose les conto acerca de sus andanzas en Schengraben exactamente como cuenta las batallas los que han tomado parte en ellas, es decir, como les gustaría, que hubieran sucedido, como lo han oído de otros narradores, como fuera más hermoso contarlas no exactamente como han sucedido.
> 
> __--- Leon Tolstoi, Guerra y paz__

> Se sentía a la cabeza de un movimiento que había comenzado y ya se había convertido en incontenible. Era com un caballo enganchado corriendo cuesta abajo. No sabia si tiraba o era arrastrado, pero avanzaba lo más rápido posible, sin tener tiempo de pensar a donde conduciría ese movimiento.
> 
> __--- Leon Tolstoi, Guerra y paz__

> Bismark esta convencido de que salió victorioso de la ultima guerra a causa de complicadas, astutas y profundas consideraciones estatales. Los miembro del Vaterland piensan que fue fruto de patriotismo. Los ingleses piensan que fueron más astutos que Napoleón y todo lo que ellos piensan no ha sido sino creado por su imaginación con ayuda de su intelecto para justificar la necesidad de derramamiento de sangre de la sociedad europea. La misma necesidad que tienen las hormigas negras y amarillas de aniquilarse las unas a otra y de construir sus hormigueros del mismo modo.
> 
> __--- Leon Tolstoi, Guerra y paz__

> En los acontecimientos históricos las grandes personalidades son las etiquetas que dan la denominación al acontecimiento pero son las que, al igual que las etiquetas, tienen una menor relación con el acontecimiento 
>  
> __--- Leon Tolstoi, Guerra y paz__




> El fuego detrás del cuarzo estaba ahora débil, encaminándose a regañadientes hacia la muerte.
>
>__--- Isaac Asimov, se puede evitar un conflicto__

> Oía el corazón. No podia imaginar que aquel leve ruido que me acompañaba desde hacia tanto tiempo pudiese cesar nunca.
>
> __--- Albert Camus, El extranjero__


> Ella parecía la única parte real de su existencia. Al lado de ella, toda la _Eternidad_ no era sino una devil fantasia por la cual no valía la pena vivir. 
> 
> __--- Isaac Asimov, El fin de la eternidad__ 

> Si queremos efecto extraños y combinaciones extraordinarias, debemos buscarlos en la vida misma que siempre llega mucho más lejos que cualquier esfuerzo de la imaginación.
> 
> __--- Sir Arthur Conan Doyle. Sherlock Holmes. La liga de los pelirrojos__

Este fragmento que sucede en dialogo entre Sherlock Holmes y Watson puede tomarse como consejo de Sir Arthur Conan Doyle que puede ser aplicado para realizar escritos de ficción  basados en eventos reales.

> It is my belief, Watson, found upon my experience , that the lowest and vilest alleys in London do not present a more dreadful record of sin than does the smiling and beautiful country side.
> 
> __--- Arthur Conan Doyle, The adventure of the Cooper Beeches__

What I find interesting about this quote is not that I find it true but rather it is telling of a way of thinking of Conan Doyle, a glimpse on how he saw and felt about the world.

Sherlock Holmes strikes as a very modern and relatable character, his life in some way resembles our modern life, he rides carriages as we would a taxi, he has meals and drinks coffee as part of his daily routine, and the fact that he has to pay rent to his land lady miss Turner are small details of normal life that makes you feel fully immerse and think of his stories as realistic as your own experiences.    

>Se equivoca, profesor. Que el vulgo crea en cometas extraordinarios que atraviesan el espacio, o en la existencia de monstruos antediluvianos que pueblan el interior del la tierra, pase, pero ni el astrónomo ni el geólogo admiten tales quimeras.
>
>__--- Julio Verne, Veinte mil leguas de viaje submarino__


> ¿Acaso no existe ninguna manera de gobernar el mundo sin hacer que la violencia sea la manera decisiva de juzgar entre el bien y el mal? ...
> vivimos en un mundo en el que la Union Soviética dispone de suficiente armamento como para mater cincuenta veces a cada habitante de la tierra, pero se siente insegura porque los Estados Unidos tienen suficiente armamento como para matar a todos sesenta veces.
> 
> __--- Isaac Asimov, No violencia. La visita del tiranosaurio- prologo Penterra__ 

> La posteridad considera una revolución triunfante como el levantamiento de una nación o de un grupo que forman un solo frente, pero la mayoría de las veces esa idea no es más que un lustre patriótico que se da al recuerdo. En toda revolución, aquellos que siguen fervorosamente la lucha hasta la muerte son una minoría, y suele haber un numero al menos igual que son decididamente antirrevolucionarios, más una mayoría efectiva que es apática y va a donde la llevan ( en una u otra dirección) si es necesario, pero prefiere que la dejen en paz
>
> __--- Isaac Asimov, Guía de la biblia nuevo testamento__

> Cuando la historia se ocupa de las personalidades, las descripciones son blancas o negra, según los intereses del escritor.
>
> __---Asimov, Segunda fundación__


> Lo que hace más importante a tu rosa es el tiempo que tú has perdido con ella
> 
> __--- Antonie De Saint Exupery. El principito__

> -los hombres de tu tierra- dijo el principito cultivan cinco mil rosas en un jardín y no encuentran  lo que buscan. -No lo encuentran nunca - le respondí - y sin embargo, lo que buscan podrían encontrarlo en una sola rosa o en poco de agua
> 
> __--- Antonie De Saint Exupery. El principito__

> The given civilization would enter a crisis to end all crisis, i.e, extinction
> 
> __--- Stanislaw Lem, Fiasco__

> How easily an isolated group of the best people can become a threat to themselves through the influence of an individual, particularly if that individual is one of whom they count, as he were made of even better stuff than they.
> 
> __--- Stanislaw Lem, Fiasco__

> In Asimov's series the need to understand history in order to construct a sustainable future becomes the pivotal theme, both on the level of narration and on the level of characters that turn their knowledge of history into action.
>
> __--- Jari Kakela__ 
> 

> Science fiction engages in representation that makes the described phenomenon unfamiliar, but as readers recognize what this defamiliarizing representation refer to, it enables them to discover new ways of thinking and thus promotes rational understanding. 
>
> __--- Jari Kakela__ 


>[Asimov] has the habit of centering his fiction in plot and clearly stating to his reader, in rather direct terms, what is happening in his story and why it is happening.
>
> __--- Jari Kakela__ 


> Asimov's plots are rarely very dramatic as such. Rather they focus on the rational, gradual discovery of what has happened (or will, or should happen) frequently resulting in detective story like narrative structures ...
>
> it is this sense of accumulating evidence, or knowledge and understanding of the world and the events described, which creates the narrative tension much of Asimov's work. 
>
> __--- Jari Kakela__ 


> Establishing the historicity of the present in the sense of denaturalizing the present by showing it to be neither arbitrary nor inevitable but the conjunctural result of complex, knowable material processes.
> 
>__Carl Freedman, Critical theory and science fiction__

> Es cierto que la nación no se crea por leyes ni decretos, sobre todo en Colombia, pues sabido es que muchas normas estipuladas en la Constitución son letra muerta
> 
> __--- María Elena Erazo, Construcción de la nación Colombiana__


> Ustedes se deleitan haciendo leyes pero más se deleitan burlándolas como los niños que jugando en la playa levantan con paciencia castillo de arena que luego destruyen entre risas.
>  
> __--- Facundo Cabral, El profeta de Gibran__

> E-government is defined as a way for governments to use the most innovative information and communication technologies, particularly web-based applications, to provide citizens and business with more convenient access to government information and services 
>  
> __--- Zhiyuan Fang, Egoverments in digital era__

> Se habla a veces de hecho, de la crueldad <<bestial>> del hombre, pero esto es terriblemente injusto y ofensivo para las bestias: una bestia nunca puede ser tan cruel como el hombre , tan artística, tan plásticamente cruel.
>
> __--- Fiodor Dostievskin, Los hermanos Karamazov__ 

> Let me tell you, my friend, that there are things done to-day in electrical science which would have been deemed unholy by the very men who discovered the electricity - who would themselves not so long before have been burned as wizards,  
>
>__--- Bram stroker, Dracula__

> El ni siquiera tomo aliento para explicar que las cucarachas, el insecto más antiguo sobre la tierra, era ya la víctima favorita de los chancletazos en el Antiguo Testamento, pero que como especie era definitivamente refractaria a cualquier método de exterminio, desde las rebanadas de tomate con boráx hasta la harina con azúcar, pues sus mil seiscientas tres variedades habían resistido a la más remota,tenaz y despiadada persecución que el hombre había desatado desde sus orígenes contra ser viviente alguno, inclusive el propio hombre, hasta el extremo de que así como se atribuía al género humano un instinto de reproducción, debía atribuírsele otro más definido y apremiante, que era el instinto de matar cucarachas, y que si éstas habían logrado escapar a la ferocidad humana era porque se habían refugiado  en las tinieblas, donde se hicieron invulnerables por el miedo congénito del hombre a la oscuridad.   
> 
>__--- Gabriel Garcia Marquez, cien años de soledad__



> -  Diremos que lo encotramos flotando en la canastilla - sonrió 
>  - No se lo creerá nadie-  dijo la monja 
> - Si se lo creyeron a las Sagradas Escritura - replico Fernanda-, no veo por qué no han de creérmelo a mi 
> 
>__--- Gabriel Garcia Marquez, cien años de soledad__


> Era en aquel momento temprano del reinado que cada pueblo vive unas cinco veces en un siglo. Un periodo  revolucionario, diferente de lo que llamamos <<revolución>> solo por el hecho de que el poder está en manos del antiguo gobierno y no del nuevo. En estas revoluciones, como en todas las demás, se habla del espíritu de los nuevos tiempos, de las exigencias de este tiempo, de los derechos del hombre, de la necesidad de que impere la sensatez en la estructura del estad y la justicia general. Bajo el pretexto de estas ideas, también entran en liza las pasiones más irrazonables del hombre. Pasarán  el tiempo y las ganas, y los antiguos introductores de lo novedoso se aferrarán a su antiguo orden nuevo, ahora anticuado, y defenderán la decoración de su casa frente a la juventud que crece, que de nuevo quiere y necesita satisfacer su necesidad de probar fuerzas. 
> 
> __--- Leon Tolstoi, Guerra y paz__

> The crusade was failing on the shore of the promised land, its prophet entrenched in its vision, its soldier-priest lost in self-doubt. In the literature describing the anthropology of religions, this is the time of scapegoats and the murderous crowd.
> 
> __---Thierry Bardin, bootstrapping Douglas Endelbart and the origins of Personal Computing__


## STEM

> Physics my friend is a narrow path drawn across a gulf that the human imagination cannot grasp
> 
> __--- Stanislaw Lem, Fiasco__

> Although the harmonic series when summed diverges it does so at an incredible slow rate as if a snail was crawling to infinity. 
>
> __--- Physics explained youtube channel__ 

> figuring out mathematical operations and tricks certainly takes significant  amounts  of  effort,  time,  and  devotion.  Today,  we  often  take  for  granted  those  symbols  and explanations that are neatly compiled into math and science textbooks. It is easy to forget that every  equation  encases  a  story:  frustration,  fascination,  arduous  work,  friendly  collaborations,  disappointment, and the occasional serendipity. Mathematics is not just about numbers, but it is also about the people whose work gives us the luxury and pleasure of understanding.  
>
> __---  Rafael Villarreal-Calderon, Chopping Logs:  A Look at the History and Uses of Logarithms__

> El trabajo de Boltzmann no negaba la vigencia de las leyes de Newton; simplemente era una forma nueva de tratar inmensos conjuntos de partículas. Esto no fue entendido bien por buena parte de sus contemporáneos, para los cuales era difícil aceptar que lo que hasta entonces se consideraban leyes fundamentales de la naturaleza, como el segundo principio de la termodinámica, pudieran tener una interpretación estadística, minando así su carácter estrictamente determinista.

> Thus a familiar model connected to physical intuition, but constituting matter of some ill-understood sort of wave, confronted an abstract mathematics with seemingly bizarra variables, insistent about discontinuity and suspending space-time pictures. Unsurprisingly, the coexistence of alternative theories generated debate. The fact, soon demonstrated, of their mathematical equivalence did not resolve the interpretative dispute. For fundamentally different physical pictures were offer.
> 
> __--- Catheryn Carson, The origins of the quantum theory__

> Talk about an equimanicla problem of epic proportions. Here is your question today. You have to understand how all of the known elements in the universe fit together in a logical way. Good luck take care
> 
> __--- Matt Walker, The lex Fridman Podcast__



### Computers
> It is time to give these users more control over their computers through education and supporting software, this will cause a massive surge in productivity and creativity, with a far-ranging impact that can barely be anticipated or imagined.
>
> __-- Guido Van Rousell, Computer Programming for Everybody__

> I want to study not only what computer is doing for us, but what it is doing to us.
>
> __--- Sherry Turkle__

> The creators of personal computer technology linked their innovations to ideologies or representations that explained and justified their designs. Those visions have become invisible, latent assumptions to the latter-day users of the personal computer, even as they shape these user' activities and attitudes. It is my task here to make them visible once again.
>  
> __---Thierry Bardin, bootstrapping Douglas Endelbart and the origins of Personal Computing__

> "To make it easy one must make coding comprehensible. Present notation  have many disadvantages; all are incomprehensible to the novice, they are all different (one for each machine) and they are never easy to read. it is quite difficult to decipher coded programs even with notes and even if you yourself made the program several months ago" (quoted in Lubar 1993,359).
>
> __---Thierry Bardin, bootstrapping Douglas Endelbart and the origins of Personal Computing__

This quote captures the problem of making code accesible and understandable, a problem that despite all the technological advances is one we are still facing today (written in 2021).


> If we then ask ourselves where that intelligence is embodied we are forced to concede that it is elusively distributed throughout hierarchy of functional processesa hierarchy whose foundation extends down into natural processes below the depth of our comprehension. If there is any one thing upon which this intelligence depends it would seem to be _organization_
>
>__--- Douglas Engelbart, Augmenting Human Intellect__ 

> The user is at first a virtuality to be invented by the designer and realized along with the technology. Or to put it the other way around, technological innovation initially entails a script that defines specific characters as its users, independent of any real actors who might take those parts.
>
> __---Thierry Bardin, bootstrapping Douglas Endelbart and the origins of Personal Computing__

> It started ... when I began work ... on the design of interactive systems to be used by office workers for document preparation. My observations of secretaries learning to use the text editors of that era soon convinced me that my beloved computers were, in fact unfriendly monsters, and that their sharpest fangs were the ever-present modes.(Tesler 1981, 90)
> 
> __---Thierry Bardin, bootstrapping Douglas Endelbart and the origins of Personal Computing__

> The future often is seen in terms of yesterday's questions, just as the past is seen in terms of what prevailed. But some of the yesterday's questions remain unanswered about the future of the personal computer, and indeed, about the future of the person who used it. Clues to what those answers might be sometimes can be found by remembering and reevaluating what did not prevail.
> 
> __---Thierry Bardin, bootstrapping Douglas Endelbart and the origins of Personal Computing__



> The great success of the internet is not technical, but in human impact. Electronic mail may not be a wonderful advance in Computer science but it is a whole new way fo people to communicate
> 
> __--- David Clark, [RFC 1336](https://datatracker.ietf.org/doc/html/rfc1336)__

### Mechatronics
> Clearly, an automobile with 30–60 microcontrollers, up to 100 electric motors, about 200 pounds of wiring, a multitude of sensors, and thousands of lines of software code can hardly be classified as a strictly mechanical system. The automobile is being transformed into a comprehensive mechatronic system.
>
> __--- Robert H. Bishop and M. K. Ramasubramanian, What is Mechatronics__

> No matter how well an individual subsystem or component (electric motor, sensor, power amplifier, or DSP) performs, the overall performance can be degraded if the designer fails to integrate and optimize the electromechanical system. 
>
> __---Sergey Edward Lyshevski, The Mechatronics handbook__

> Modeling, simulation, analysis, virtual prototyping, and visualization are critical and urgently important aspects for developing and prototyping of advanced electromechanical systems.
> 
> __---Sergey Edward Lyshevski, The Mechatronics handbook__



<!---
{{< youtube PL1-Z9ArJBD7tBImjZDuETByCk3q9djTMc >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL1-Z9ArJBD7tBImjZDuETByCk3q9djTMc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
--->

## Organization

> While an ideal time to begin a company handbook is at inception, the next best time is _today_.
>
>__--- [gitlab Handbook](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/)__




## Education and learning

> The sheer magnitude of human knowledge renders its coverage by education an impossibility, rather. The goal of education is better conceived as helping students develop the intellectual tools and learning strategies needed to adquiere the knowledge that allows people to think productively. 
>
> __--- John D. Bransford, Ann L. Brown y Rodney R. Cocking, How people learn:  Brain, Mind, Experience, and School__


> The complexity of social life requires that the same problems be studied many times before basic uniformities can be found differentiated from transitory social occurrences.
>
> __--- The people's choice: How the Voter Makes Up His Mind in a Presidential Campaign__


> Every individual carries around with him germs of observations and half-forgotten experiences.
>
> __--- The people's choice: How the Voter Makes Up His Mind in a Presidential Campaign__

> If you want to build a ship don't drum up people to collect wood don't assign them tasks and work, but rather teach them to long for the endless immensity of the sea.
> 
> __--- Antoine de Saint Exupéry__


> Aunque el mundo en conjunto progrese, la juventud tiene que volver a empezar por el principio y pasar, como individuo, por las épocas de la cultura de la humanidad
> 
> __--- Goethe__


> If you really want to lean and get better at anything ... you'll have to be willing to be uncomfortable because thinking takes effort it involves  fighting through confusion.
>
> __--- Dereck Muller - Veritasium__
 

> Un mundo le es dado al hombre; su gloria no es soportar ni despreciar este mundo, sino enriquecerlo construyendo otros universos.
> 
> __--- Mario Bunge, La ciencia su método y su filosofía__

> if a lion could speak, we would not be able to understand him
> 
> __--- Lugwig Wittgenstein__

Context has a key role in communication 

> On one hand information wants to be expensive because it's so valuable. The right information in the right place, just changes your life. On the other hand, information wants to be free, because the cost of getting it out is getting lower and lower all the time. So you have these two fighting each other. 
>
> __--- Stewart Brand__

> it always appeared logically and aesthetically simpler to assume constancy instead of variability.
>
> __--- Milic Capek, The philosophical impact of contemporary physics__


> The path of true learning is a strewn with rocks not roses. 
> 
> __--- Mortimer Adler__

> I remember in high school during first period a guy would come to me with a puzzle ... I wouldn't stop until i figure the damn thing out it would take me 15 or 20 minutes but during the day other guys would come to me and I'd do it for them in a flash. So for a guy, to do it took me 20 minutes while there were five who thought I was a super genius
>  
> __--- Surely you're joking mr. Feynman__

> "Is there a way to see it". ALL the time you're saying to yourself "I could do that but I won't" which is just another way of saying that you can't
>  
> __--- Surely you're joking mr. Feynman__

> The efective motivation to continue learning can be fostered only by leading students to experience the pleasure inherent in solving a problem seen and chosen as one's own.
>
>__--- A constructivist approach to teaching, Ernst von Glasersfeld__


> One dimension of acquiring greater competence appears to be the increased ability to segment the perceptual field (learning how to see)...
> 
> Sometimes, however students can solve set of practice problems but fail to conditionalize their knowledge because they know which chapter the problems came from ad so automatically use this information to decide which concepts and formulas are relevant.
>
> __--- John D. Bransford, Ann L. Brown y Rodney R. Cocking, How people learn:  Brain, Mind, Experience, and School__

> Thinking an expert as "someone who knows all the answers" is very hurtful because it place severe constraints on new learning and create the tendency to worry about looking competent rather than publicly acknowledging the need for help in certain areas.
>
> __--- John D. Bransford, Ann L. Brown y Rodney R. Cocking, How people learn:  Brain, Mind, Experience, and School__

> The ability to recognize the limits of one's current knowledge, then take steps to remedy the situation, is extremely important for learners at all ages.
>
> __--- John D. Bransford, Ann L. Brown y Rodney R. Cocking, How people learn:  Brain, Mind, Experience, and School__

> Learners of all ages are more motivated when they can see the usefulness of what they are learning and when they can use that information to do something that has an impact on others
>
> __--- John D. Bransford, Ann L. Brown y Rodney R. Cocking, How people learn:  Brain, Mind, Experience, and School__

> There is a risk of giving people the illusion of understanding math .. unless they solve a problem that requires struggle ... I don't have a lot of faith that actual learning took place.
>
> __--- Grant Sanderson (three blue on brown), Showmakers podcast__


> La educación se ve obligada a proporcionar las cartas náuticas de un mundo complejo y en perpetua agitación y, al mismo tiempo, la brújula para poder navegar por él.
>
>_--- Jackes Delors, Los cuatro pilares de la educación__



## Economy 

> El producido se divide en 3 partes la renta, los salarios y los beneficios del capital
>
>__--- Adam Smith, La riqueza de las naciones__

> Ellos cuyo ingreso no les cuesta ni trabajo ni preocupaciones; puede decirse que acude a sus manos espontáneamente, sin que ellos elaboren plan ni proyecto alguno con tal objetivo. 
> Esa indolencia que es el efecto natural de su posición tan cómoda y segura, los vuelve con mucha frecuencia no solo ignorantes sino incapaces del ejercicio intelectual necesario para prever y comprender las consecuencias de cualquier reglamentación publica. 
>
>__--- Adam Smith, La riqueza de las naciones__

Haciendo una critica al modelo mercantilista Adam Smith, pensando en el oro y la plata (dinero) como un instrumento de intercambio y no como la fuente de la riqueza, los compara con ollas y sartenes expresando que su acumulación no genera ningún valor pues: 

> Fácilmente se comprende sin embargo, que el numero de esto utensilios esta limitado en cualquier lugar por el uso que se pueda hacer de ellos...
> debería comprenderse de forma igualmente inmediata que la cantidad de oro y plata esta limitado en cualquier país por el uso que se pueda hacer de ellos.
>
>__--- Adam Smith, La riqueza de las naciones__

> cuando hay grandes propiedades hay grandes desigualdades. Por cada hombre muy rico debe haber por lo menos 500 pobres, y la opulencia de unos pocos supone la indigencia de muchos. La abundancia de los ricos aviva la indignación de los pobres, que son conducidos por necesidad y alentados por la envidia a atropellar sus posesiones. El dueño de toda propiedad valiosa no puede dormir seguro ni una sola noche si no se halla bajo la protección de un magistrado civil. Todo el tiempo se ve rodeado por enemigos desconocidos a quienes nunca a provocado pero a quienes nunca puede apaciguar jamás.
>
>__--- Adam Smith, La riqueza de las naciones__

> Ahorrar, ahorra sin cesar, acumular para acumular, producir para producir; ese es el lema capitalista burgués; el proletariado no es más que una maquina que produce plusvalia, el capitalista es una maquina que capitaliza esta plusvalia.
> 
> __--- Karl Marx, El capital__

> The economy's not a car, there's no engine to stall, no expert can fix it, there's not it at all. The economy is us we don't need a mechanic put away the wrenches the economy is organic.
>
>[Fight of the Century: Keynes vs. Hayek - Economics Rap Battle Round Two](https://www.youtube.com/watch?v=GTQnarzmTOc)


> If I say that the first industrial revolution, the revolution of the 'dark Satanic mills', was the __devaluation of the human arm__ by the competition of machinery there is not rate of pay at which the United States pick an shovel laborer can live which  is low enough to compete with the work of a steam shovel as an excavator.
>
> The modern industrial revolution is similarly bound to __devalue the human brain__ at least in its simpler or more routine decisions. Of course, just as the skilled carpenter, the skilled mechanic, the skilled dressmaker have in some degree survived the first industrial revolution, so the skilled scientist and the skilled administrator may survive the second. However, taking the second revolution as accomplished, the average human being of mediocre attainments or less has nothing to sell that it is worth anyone’s money to buy.
>
>The answer, of course, is to have a society based on human values other than buying or selling. To arrive at this society, we need a good deal of planning and a good deal of struggle
>
> __--- Robert wiener, Cybernetics__

> The history of industrial automation is characterized by periods of rapid change in
popular methods. Either as a cause or, perhaps, an effect, such periods of change in
automation techniques seem closely tied to world economics
>
>__---- John Craig__



it is fascinating to see how an indicator of economic prosperity has shifted from production to consumption

## Meaning 

> Lo que necesito es una compensación; de lo contrario, desaparece. Y no una compensación en cualquier parte, en el infinito, sino aquí abajo, una compensación que yo pueda ver. Yo he creído, y quiero ser testigo del resultado, y si entonces ya he muerto, que me resuciten.
> Seria muy triste que todo ocurriese sin que yo lo percibiera. No quiero que mi cuerpo, con sus sufrimientos y sus faltas, sirva tan solo para contribuir a la armonía futura en beneficio de no se quien. Quiero ver con mis propios ojos a la cierva durmiendo junto al león, a la victima besando a su matador. Sobre este deseo reposan todas las religiones y yo tengo fe.
> Quiero estar presente cuando todos se enteren del porque de las cosas. 
> 
>__--- Fiodor Dostoievski, Los hermanos Kramazov - Rebeldia__

> La vida no es lo que uno vivió sino la que uno recuerda y cómo la recuerda para contarla.
>
> __--- Gabriel Garcia Marquez, vivir para contarla__

> Still couldn't accept that meaning and solace aren't to be found on the heavens but in the tranches of everyday living
>
> __--- exurb2, And then we'll be ok__

> We are recording things about the world, we are learning things and writing stories and preserving that, is truly what I think is the essence of being a human, we are autobiographers of the universe.
> 
> __--- Michel Stevens (Vsauce), The artificial intelligence podcast__

> Toda una serie de pacientes disipulos se convirtieron en su momento en los maestro dogmáticos de la siguiente generación servil. 
>
> __--- Carl Sagan, el mundo y sus demonios__

>En una vida corta e incierta parece cruel hacer algo que pueda privar a la gente del consuelo de la fe cuando la ciencia no puede remediar su angustia.
>
> __--- Carl Sagan, el mundo y sus demonios__

> He oído alguna vez a un escéptico que se creyera superior y despreciativo? Sin duda. A veces incluso he oído ese ton desagradable y me aflige recordarlo en mi propia voz.
>
> __--- Carl Sagan, el mundo y sus demonios__

> May be I don't have enough to do. May be I have time to think too much.
>
> __---Ray Bradbury, The veldt__

> It is not good for a man to be cognizant of his physical and spiritual mechanisms. Complete knowledge reveals limits to human possibilities and the less a man is by nature in his purposes, the lees he can tolerate limits.
>
>__--- Stanislaw Lem, Fiasco__

> The fact that you are not happy has nothing to do with what you have or don't have. the fact that you are not happy is your  life doesn't match the way you think it should be. And you have some idea how you think it should be.


> Un mundo le es dado al hombre; su gloria no es soportar o despreciar este mundo, sino enriquecerlo construyendo otros universos
>
> __--- Mario Bunge, La ciencia su método y su filosofía__


[Randy Pausch Last Lecture: Achieving Your Childhood Dreams](https://youtu.be/ji5_MqicxSo)
[John Danaher at the Lex Fridman Podcast](https://www.youtube.com/watch?v=ktuw6Ow4sd0)

### Time by Pink Floyd
{{< youtube JwYX52BP2Sk >}}


## Sayings 
> Meaning is jumper that you have to knit yourself

> El sueño de la razón produce monstros
> 
>__--- Francisco de Goya__

> What a delightful crap bag life is 
>  
> __---Jason Scott, Jason Scott talks his way out podcast__

> Engañar esta mal aunque el diablo sea la victima 
> 
> __--- Isaac Asimov__

> In theory, there is no difference between theory and practice, but in practice there is
> 
> __---Benjamin Brewster, earliest Known appearance__

> Instant gratification is our society's motto

> Everyone you will ever meet knows something you don't
>
> __--- bill Nye__ 

>  If you are not paying for the product you are the product 

> El azar nunca es azar, ya que solo favorece a los ojos preparados

> What do you fear? the unstoppable marching of time that is slowly guiding us all towards an inevitable death

> 80% of success is showing up
> 
> __--- Woody Alen__

> When a measure becomes a target it ceases to be a good measure

> We're just in a long attentional free fall, with no clear destination and all manner of strange things flashing past.
> 
> __--- Katheyn Schulz, The New Yorker__

> Si uno esta en un barco que naufrago pensara en naufragios. 
> 
> __--- George Orwell__

> It is very sad to mee that some people are so intent on leaving their mark on the world that they don't care if that mark is a scar
>
>  __--- Hank Green__
