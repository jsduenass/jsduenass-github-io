---
title: "WatchList"
date: 2020-10-27
description: List of books to read  
menu:
  sidebar:
    name: "Watch list"
    identifier: watchlist
    parent: library    
    weight: 22

---

## Reading list
- [] [The MIT Rad Lab Series](http://web.mit.edu/klund/www/books/radlab.html). 
- [] China 2185. liu cixin
- [] Cibernetica. Norbert weiner 
- [] The Human Use of Human Beings. Norbert weiner
- [] [Baumgartner & Momsen Detective Stories for German Learners](https://books.learnoutlive.com/learning-german-storytelling-baumgartner-momsen-detective-stories-german-learners-collectors-edition-1-5/?edition=ebook)
- [] La historia de mi vida. Hellen Keller
- [] El límite de las mentiras: La polémica vida del Perito Francisco Pascasio Moreno 1852-1919. Gerardo Bartolomé
- [] Emilio o de la educación. Rousseau.
- [] Historia social del conocimiento. Peter Burke
- [] The Feynman Lectures on Physics
- [] this is how they tell me the world ends. Nicole Perlroth
- [] Self-Reliance. Ralph Waldo Emerson
- [] Si una Noche de Invierno un Viajero. Italo Calvino 
- [] where wizards stay up late. Katie Hafner
- [] Golem. Stanislaw Lem
- [] My Philosophy of Industry. Henry Ford
- [] Hard drive bill gates and the making of the microsoft empire
- [] Pale Blue Dot. Carl Sagan
- [] Fish is Fish. Leo Lionni. Children's book
- [] The reluctant dragon
- [] The walking drum. Louis L. Amoun
- [] Introduction to quantum mechanics. David Griffiths
- [] The time machine. H.G wells
- [] How to solve it. George Poyla
- [] Los años de aprendizaje de Wilhelm Meister. Wolfgang Goethe 
- [] En busca del tiempo perdido. Marcel Proust
- [] How we learn. Benedic Carey
- [] Armas de instrucción masiva. John Taylor Gatto
- [] The pig that wants to be eaten. Julian Baggani
- [] El mito de sisifo. Albert camus
- [] La Revolución Historiográfica Francesa. Peter Burke
- [] Por las grutas y selvas del Indostán. Helena Blavatsky
- [] Diagnosing the System for Organizations. Stafford Beer 
- [] Social Commitments in a Depersonalized World. Shane R. Thye  
- [] Dynamics: the Geometry of Behavior. Ralph Abraham
- [] Computers as theatre. Brenda Laurel 
- [] Modern Operating Systems. Stuart Tanenbaum 
- [] Computer networks. Stuart Tanenbaum
- [] Oryx and crake. Margaret Atwood
- [] The selfish gene.Richard Dawkins
- [] On the origin of species. Charles Darwin
- [] the Dragon Book


### Books read

> I cannot remember the books I've read anymore than the meal I have eaten; even so, they have made me 
>
> __--- Ralph Waldo Emerson__

- [x] Cuentos completos I. Isaac Asimov
- [x] Fundación. Isaac Asimov
- [x] Los hermanos kamarazov. Fyodor Dostoyevsky.   [The Problem of Evil: Crash Course Philosophy](https://www.youtube.com/watch?v=9AzNEG1GB-k)  
- [x] La guia del autoestopista intergaláctico. Douglas Adams
- [x] La nave de un millón de años. Poul Anderson
- [x] Visiones de robot. Isaac Asimov
- [x] The hunger games. Suzanne Collins
- [x] La traición de Darwin. Gerardo Bartolomé
- [x] Fiasco. Stanislaw Lem 
- [x] Historia del futuro. Robert Anson Heinlein [extra sci-fi](https://www.youtube.com/watch?v=XaWMe5nC9SA)  
- [x] Ensayo sobre la ceguera. José Saramago 
- [x] Los propios dioses.Isaac Asimov
- [x] Bóvedas de acero. Isaac Asimov
- [x] El sol desnudo. Asimov
- [x] Bootstrapping Douglas Endelbart and the origins of Personal Computing. Thierry Bardin
- [x] París. Edward Rutherfurd
- [x] A tale of two cities. Charles Dickens 
- [x] Paris del siglo xx. Julio Verne
- [x] Surely you´re joking, Mr. Feynman. Feynman, Richard Phillips
- [x] Frankenstein o el moderno prometeo. Marry Shelly
- [x] Los dragones del edén. Carl Sagan
- [x] El mundo y sus demonios. Carl Sagan 
- [x] Terraformar la tierra. Jack Williamson
- [x] Guerra y paz. Leon Tolsoi
- [x] Vivir para contarla. Gabriel Garcia Marquez
- [x] El otoño del patriarca. Gabriel Garcia Marquez
- [x] Cien años de soledad. Gabriel Garcia Marquez
- [x] Spies of No Country. Matti Friedman 
- [x] El problema de los 3 cuerpos. Liu Cixin
- [x] El fin de la eternidad. Isaac Asimov
- [x] Objetivo: la luna: la historia inédita de la mayor aventura humana. Parry Dan 
- [x] Las aventuras de Sherlock Holmes. Sir arthur Connan Doyle
- [x] The narrative Life of Frederick Douglass. Frederick Douglass
- [x] El extranjero. Albert Camus 
- [x] Can Quantum-Mechanical Description of Physical Reality Be Considered Complete?. Einstein-Podolsky-Rosen. (EPR article/ EPR paradox)
- [x] La riqueza de las naciones. Adam Smith
- [x] Guía Asimov para la Biblia. Isaac Asimov
- [x] Ensayo sobre la lucidez. José Saramago
- [x] El idiota. Fyodor Dostoyevsky
- [x] [Dino lernt deutsch](https://books.learnoutlive.com/category/dino-lernt-deutsch/)
SpellRight  


